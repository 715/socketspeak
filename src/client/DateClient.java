package client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by khada on 12/19/16.
 * Hello, world!
 */
public class DateClient {
    public static void main(String args[]) throws Exception {
        Socket soc = new Socket(args[0], Integer.valueOf(args[1]));
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        soc.getInputStream()
                )
        );

        System.out.println(in.readLine());
    }
}
