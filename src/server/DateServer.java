//package server;
//
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.util.Date;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
///**
// * Created by khada on 12/19/16.
// * Hello, world!
// */
//public class DateServer {
//    private static void tellDate() throws IOException {
//        ServerSocket s = new ServerSocket(5000);
//        while (true) {
//            System.out.println("Waiting For Connection ...");
//            Socket soc = s.accept();
//            DataOutputStream out = new DataOutputStream(soc.getOutputStream());
//            String output = String.format("Server Date%s\n", new Date().toString());
//            System.out.println(output);
//            out.writeBytes(output);
//            out.close();
//            soc.close();
//        }
//    }
//    public static void main(String[] args) throws IOException {
//        ExecutorService executorService = Executors.newFixedThreadPool(100);
//        while(true){
//            executorService.execute(new Runnable() {
//                public void run() {
//                    try {
//                        tellDate();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        executorService.shutdown();
//                    }
//                }
//            });
//        }
//    }
//}