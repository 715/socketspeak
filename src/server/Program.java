package server;

/**
 * Created by khada on 12/20/16.
 * Hello, world!
 */
public class Program {
    public static void main(String[] args) {
        MultiThreadedServer threeThousandServer = new MultiThreadedServer(3000);
        new Thread(threeThousandServer).start();

        try {
            Thread.sleep(20 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stopping Server");
        threeThousandServer.stop();
    }
}
